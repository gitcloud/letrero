----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:04:20 01/14/2016 
-- Design Name: 
-- Module Name:    decodificador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decodificador is

    Port ( Q_disp : in  STD_LOGIC_VECTOR (1 downto 0);
           Q_led : out  STD_LOGIC_VECTOR (3 downto 0)
			  );
			  
end decodificador;

architecture Behavioral of decodificador is

	signal r: STD_LOGIC_VECTOR(1 downto 0);
begin
		r<=Q_disp;
		with r select      
		Q_led<="1110" when "00",
				 "1101" when "01",
				 "1011" when "10",
				 "0111" when others;

end Behavioral;

