----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:09:54 01/17/2016 
-- Design Name: 
-- Module Name:    desplazamiento - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.STD_LOGIC_arith.ALL;
use ieee.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity desplazamiento is
    Port ( clk : in  STD_LOGIC;
			  reset : in  STD_LOGIC;
           e0 : in  STD_LOGIC_VECTOR (4 downto 0);
           e1 : in  STD_LOGIC_VECTOR (4 downto 0);
           e2 : in  STD_LOGIC_VECTOR (4 downto 0);
           e3 : in  STD_LOGIC_VECTOR (4 downto 0);
           e4 : in  STD_LOGIC_VECTOR (4 downto 0);
           e5 : in  STD_LOGIC_VECTOR (4 downto 0);
           e6 : in  STD_LOGIC_VECTOR (4 downto 0);
           e7 : in  STD_LOGIC_VECTOR (4 downto 0);
           e8 : in  STD_LOGIC_VECTOR (4 downto 0);
           e9 : in  STD_LOGIC_VECTOR (4 downto 0);
           e10 : in  STD_LOGIC_VECTOR (4 downto 0);
           e11 : in  STD_LOGIC_VECTOR (4 downto 0);
           e12 : in  STD_LOGIC_VECTOR (4 downto 0);
           e13 : in  STD_LOGIC_VECTOR (4 downto 0);
           e14 : in  STD_LOGIC_VECTOR (4 downto 0);
           e15 : in  STD_LOGIC_VECTOR (4 downto 0);
           e16 : in  STD_LOGIC_VECTOR (4 downto 0);
           e17 : in  STD_LOGIC_VECTOR (4 downto 0);
           e18 : in  STD_LOGIC_VECTOR (4 downto 0);
           e19 : in  STD_LOGIC_VECTOR (4 downto 0);
           e20 : in  STD_LOGIC_VECTOR (4 downto 0);
           e21 : in  STD_LOGIC_VECTOR (4 downto 0);
           e22 : in  STD_LOGIC_VECTOR (4 downto 0);
           e23 : in  STD_LOGIC_VECTOR (4 downto 0);
           e24 : in  STD_LOGIC_VECTOR (4 downto 0);
           y0 : out  STD_LOGIC_VECTOR (4 downto 0);
           y1 : out  STD_LOGIC_VECTOR (4 downto 0);
           y2 : out  STD_LOGIC_VECTOR (4 downto 0);
           y3 : out  STD_LOGIC_VECTOR (4 downto 0)
			  );
end desplazamiento;

architecture Behavioral of desplazamiento is
	signal l0: STD_LOGIC_VECTOR (4 downto 0);
	signal l1: STD_LOGIC_VECTOR (4 downto 0);
	signal l2: STD_LOGIC_VECTOR (4 downto 0);
	signal l3: STD_LOGIC_VECTOR (4 downto 0);
	signal l4: STD_LOGIC_VECTOR (4 downto 0);
	signal l5: STD_LOGIC_VECTOR (4 downto 0);
	signal l6: STD_LOGIC_VECTOR (4 downto 0);
	signal l7: STD_LOGIC_VECTOR (4 downto 0);
	signal l8: STD_LOGIC_VECTOR (4 downto 0);
	signal l9: STD_LOGIC_VECTOR (4 downto 0);
	signal l10: STD_LOGIC_VECTOR (4 downto 0);
	signal l11: STD_LOGIC_VECTOR (4 downto 0);
	signal l12: STD_LOGIC_VECTOR (4 downto 0);
	signal l13: STD_LOGIC_VECTOR (4 downto 0);
	signal l14: STD_LOGIC_VECTOR (4 downto 0);
	signal l15: STD_LOGIC_VECTOR (4 downto 0);
	signal l16: STD_LOGIC_VECTOR (4 downto 0);
	signal l17: STD_LOGIC_VECTOR (4 downto 0);
	signal l18: STD_LOGIC_VECTOR (4 downto 0);
	signal l19: STD_LOGIC_VECTOR (4 downto 0);
	signal l20: STD_LOGIC_VECTOR (4 downto 0);
	signal l21: STD_LOGIC_VECTOR (4 downto 0);
	signal l22: STD_LOGIC_VECTOR (4 downto 0);
	signal l23: STD_LOGIC_VECTOR (4 downto 0);
	signal l24: STD_LOGIC_VECTOR (4 downto 0);
	signal s0,s1,s2,s3: std_logic_vector(4 downto 0);
	signal count: integer := 0;
	
begin
	l0<=e0;
	l1<=e1;
	l2<=e2;
	l3<=e3;
	l4<=e4;
	l5<=e5;
	l6<=e6;
	l7<=e7;
	l8<=e8;
	l9<=e9;
	l10<=e10;
	l11<=e11;
	l12<=e12;
	l13<=e13;
	l14<=e14;
	l15<=e15;
	l16<=e16;
	l17<=e17;
	l18<=e18;
	l19<=e19;
	l20<=e20;
	l21<=e21;
	l22<=e22;
	l23<=e23;
	l24<=e24;
	
process(clk, reset)
		begin
			if reset = '1' then
				count <=  0;
			
		
			elsif rising_edge(clk) then
				case count is
					when 0 =>
						s0<=l0;
						s1<=l1;
						s2<=l2;
						s3<=l3;
					when 1 =>
						s0<=l1;
						s1<=l2;
						s2<=l3;
						s3<=l4;
					
					when 2 =>
						s0<=l2;
						s1<=l3;
						s2<=l4;
						s3<=l5;
					when 3 =>
						s0<=l3;
						s1<=l4;
						s2<=l5;
						s3<=l6;
					when 4 =>
						s0<=l4;
						s1<=l5;
						s2<=l6;
						s3<=l7;
					when 5 =>
						s0<=l5;
						s1<=l6;
						s2<=l7;
						s3<=l8;
					when 6 =>
						s0<=l6;
						s1<=l7;
						s2<=l8;
						s3<=l9;
					when 7 =>
						s0<=l7;
						s1<=l8;
						s2<=l9;
						s3<=l10;
					when 8 =>
						s0<=l8;
						s1<=l9;
						s2<=l10;
						s3<=l11;
					when 9 =>
						s0<=l9;
						s1<=l10;
						s2<=l11;
						s3<=l12;
					when 10 =>
						s0<=l10;
						s1<=l11;
						s2<=l12;
						s3<=l13;
					when 11 =>
						s0<=l11;
						s1<=l12;
						s2<=l13;
						s3<=l14;
					when 12 =>
						s0<=l12;
						s1<=l13;
						s2<=l14;
						s3<=l15;
					when 13 =>
						s0<=l13;
						s1<=l14;
						s2<=l15;
						s3<=l16;
					when 14 =>
						s0<=l14;
						s1<=l15;
						s2<=l16;
						s3<=l17;
					when 15 =>
						s0<=l15;
						s1<=l16;
						s2<=l17;
						s3<=l18;
					when 16 =>
						s0<=l16;
						s1<=l17;
						s2<=l18;
						s3<=l19;
					when 17 =>
						s0<=l17;
						s1<=l18;
						s2<=l19;
						s3<=l20;
					when 18 =>
						s0<=l18;
						s1<=l19;
						s2<=l20;
						s3<=l21;
					when 19 =>
						s0<=l19;
						s1<=l20;
						s2<=l21;
						s3<=l22;
					when 20 =>
						s0<=l20;
						s1<=l21;
						s2<=l22;
						s3<=l23;
					when 21 =>
						s0<=l21;
						s1<=l22;
						s2<=l23;
						s3<=l24;
					when 22 =>
						s0<=l22;
						s1<=l23;
						s2<=l24;
						s3<=l0;
					when 23 =>
						s0<=l23;
						s1<=l24;
						s2<=l0;
						s3<=l1;
					when 24 =>
						s0<=l24;
						s1<=l0;
						s2<=l1;
						s3<=l2;
						
					when others => 
						s0<="11111";
						s1<="11111";
						s2<="11111";
						s3<="11111";
					
				end case;
				count <= count + 1;
				if count = 24 then count<=0;
				end if;
				
			end if;
--				if count="11000" then
--					count<="00000";
--				
--					count<=count+"00001";
--				end if;
--			end if;
	--		if count="11000" then
--				y0<=l4;
--				y1<=l5;
--				y2<=l6;
--				y3<=l7;
--				count <= count - 1;
----			elsif count="10111" then
--				y0<=l1;
--				y1<=l2;
--				y2<=l3;
--				y3<=l4;
--				count<=count-"00001";
--			elsif count="10110" then
--				y0<=l2;
--				y1<=l3;
--				y2<=l4;
--				y3<=l5;
--				count<=count-"00001";--"10110";
--			elsif count="10101" then
--				y0<=l3;
--				y1<=l4;
--				y2<=l5;
--				y3<=l6;
--				count<=count-"00001";
--			elsif count="10100" then
--				y0<=l4;
--				y1<=l5;
--				y2<=l6;
--				y3<=l7;
--				count<=count-"00001";
--			end if;
--			if count="10011" then
--				y0<=l5;
--				y1<=l6;
--				y2<=l7;
--				y3<=l8;
--				count<=count-"00001";
--			end if;
--			if count="10010" then
--				y0<=l6;
--				y1<=l7;
--				y2<=l8;
--				y3<=l9;
--				count<=count-"00001";
--			end if;
--			if count="10001" then
--				y0<=l7;
--				y1<=l8;
--				y2<=l9;
--				y3<=l10;
--				count<=count-"00001";
--			end if;
--			if count="10000" then
--				y0<=l8;
--				y1<=l9;
--				y2<=l10;
--				y3<=l11;
--				count<=count-"00001";
--			end if;
--			if count="01111" then
--				y0<=l9;
--				y1<=l10;
--				y2<=l11;
--				y3<=l12;
--				count<=count-"00001";
--			end if;
--			if count="01110" then
--				y0<=l10;
--				y1<=l11;
--				y2<=l12;
--				y3<=l13;
--				count<=count-"00001";
--			end if;
--			if count="01101" then
--				y0<=l11;
--				y1<=l12;
--				y2<=l13;
--				y3<=l14;
--				count<=count-"00001";
--			end if;
--			if count="01100" then
--				y0<=l12;
--				y1<=l13;
--				y2<=l14;
--				y3<=l15;
--				count<=count-"00001";
--			end if;
--			if count="01011" then
--				y0<=l13;
--				y1<=l14;
--				y2<=l15;
--				y3<=l16;
--				count<=count-"00001";
--			end if;
--			if count="01010" then
--				y0<=l14;
--				y1<=l15;
--				y2<=l16;
--				y3<=l17;
--				count<=count-"00001";
--			end if;
--			if count="01001" then
--				y0<=l15;
--				y1<=l16;
--				y2<=l17;
--				y3<=l18;
--				count<=count-"00001";
--			end if;
--			if count="01000" then
--				y0<=l16;
--				y1<=l17;
--				y2<=l18;
--				y3<=l19;
--				count<=count-"00001";
--			end if;
--			if count="00111" then
--				y0<=l17;
--				y1<=l18;
--				y2<=l19;
--				y3<=l20;
--				count<=count-"00001";
--			end if;
--			if count="00110" then
--				y0<=l18;
--				y1<=l19;
--				y2<=l20;
--				y3<=l21;
--				count<=count-"00001";
--			end if;
--			if count="00101" then
--				y0<=l19;
--				y1<=l20;
--				y2<=l21;
--				y3<=l22;
--				count<=count-"00001";
--			end if;
--			if count="00100" then
--				y0<=l20;
--				y1<=l21;
--				y2<=l22;
--				y3<=l23;
--				count<=count-"00001";
--			end if;
--			if count="00011" then
--				y0<=l21;
--				y1<=l22;
--				y2<=l23;
--				y3<=l24;
--				count<=count-"00001";
--			end if;
--			if count="00010" then
--				y0<=l22;
--				y1<=l23;
--				y2<=l24;
--				y3<=l0;
--				count<=count-"00001";
--			end if;
--			if count="00001" then
--				y0<=l23;
--				y1<=l24;
--				y2<=l0;
--				y3<=l1;
--				count<=count-"00001";
--			end if;
--			if count="00000" then
--				y0<=l24;
--				y1<=l0;
--				y2<=l1;
--				y3<=l2;
--				count<="11000";
--			end if;
	
	--		end if;
			--end if;
	end process;
	
	y0 <= s0;
	y1 <= s1;
	y2 <= s2;
	y3 <= s3;
	
end Behavioral;

