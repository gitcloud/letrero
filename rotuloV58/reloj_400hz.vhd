----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:56:50 01/21/2016 
-- Design Name: 
-- Module Name:    reloj_400hz - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.STD_LOGIC_arith.ALL;
use ieee.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity reloj_400hz is

    Port ( reset : in  STD_LOGIC;
           clk_in : in  STD_LOGIC;
           clk_1hz : out  STD_LOGIC);
end reloj_400hz;


architecture Behavioral of clk_divider is
signal temp: std_logic;
signal counter: integer range 0 to 62499;

begin

    clk_1hz <=temp;
    process(reset, clk_in)
    begin
        if rising_edge(clk_in) then
           if reset='1' then    
                temp <= '0';
                counter <= 0;

            else 
                if counter = 62499 then 
                    temp <= not (temp);
                   counter <= 0;

                else
                    counter <= counter + 1;
                end if;
            end if;
        end if;
    end process;
end Behavioral;
