----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:08:48 01/14/2016 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.STD_LOGIC_arith.ALL;
use ieee.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           selected : in  STD_LOGIC;
           up : in  STD_LOGIC;
           down : in  STD_LOGIC;
			  mode: in STD_LOGIC;
			  segmentos :out STD_LOGIC_VECTOR(6 downto 0);
			  ctrl: out STD_LOGIC_VECTOR(3 downto 0)
			  );
end top;

architecture Behavioral of top is

	signal cuenta:STD_LOGIC_VECTOR(4 downto 0):="00000";
	signal dir_memoria:STD_LOGIC_VECTOR(4 downto 0):="00100";     
	signal coord:integer range 0 to 10;     
	
	signal signal1:STD_LOGIC_VECTOR (1 downto 0);  --se�al para conectar q_disp del visualizador con el q_disp del decodificador
	signal signal2:STD_LOGIC_VECTOR (4 downto 0); --conecta las se�ales de control de mux_datos 
	signal signal3:STD_LOGIC_VECTOR (4 downto 0);-- conecta las se�ales de q_mux del visualizador
	signal signalMUX:STD_LOGIC_VECTOR (4 downto 0);-- conecta las se�ales de q_mux del visualizador

	signal s_datos0:STD_LOGIC_VECTOR (4 downto 0);--:="11111"; --conectan las salidas de datos de demux con las entras de la memoria de visualizador
	signal s_datos1:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos2:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos3:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos4:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos5:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos6:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos7:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos8:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos9:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos10:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos11:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos12:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos13:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos14:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos15:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos16:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos17:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos18:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos19:STD_LOGIC_VECTOR (4 downto 0);--:--="11111";
	signal s_datos20:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos21:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos22:STD_LOGIC_VECTOR (4 downto 0);--:="11111";
	signal s_datos23:STD_LOGIC_VECTOR (4 downto 0);--:-="11111";
	signal s_datos24:STD_LOGIC_VECTOR (4 downto 0);--:="11111";

signal flanco_up :std_logic:='0';
signal flanco_selected :std_logic:='0';
--signal flanco_mode :std_logic:='0';
     

	component demux 
		Port (  
				  s0 : in  STD_LOGIC;
				  s1 : in  STD_LOGIC;
				  s2 : in  STD_LOGIC;
				  s3 : in  STD_LOGIC;
				  s4 : in  STD_LOGIC;
				  b : in  STD_LOGIC_VECTOR (4 downto 0);
				  y0 : out STD_LOGIC_VECTOR (4 downto 0);
				  y1 : out STD_LOGIC_VECTOR (4 downto 0);
				  y2 : out STD_LOGIC_VECTOR (4 downto 0);
				  y3 : out STD_LOGIC_VECTOR (4 downto 0);
				  y4 : out STD_LOGIC_VECTOR (4 downto 0);
				  y5 : out STD_LOGIC_VECTOR (4 downto 0);
				  y6 : out STD_LOGIC_VECTOR (4 downto 0);
				  y7 : out STD_LOGIC_VECTOR (4 downto 0);
				  y8 : out STD_LOGIC_VECTOR (4 downto 0);
				  y9 : out STD_LOGIC_VECTOR (4 downto 0);
				  y10 : out STD_LOGIC_VECTOR (4 downto 0);
				  y11 : out STD_LOGIC_VECTOR (4 downto 0);
				  y12 : out STD_LOGIC_VECTOR (4 downto 0);
				  y13 : out STD_LOGIC_VECTOR (4 downto 0);
				  y14 : out STD_LOGIC_VECTOR (4 downto 0);
				  y15 : out STD_LOGIC_VECTOR (4 downto 0);
				  y16 : out STD_LOGIC_VECTOR (4 downto 0);
				  y17 : out STD_LOGIC_VECTOR (4 downto 0);
				  y18 : out STD_LOGIC_VECTOR (4 downto 0);
				  y19 : out STD_LOGIC_VECTOR (4 downto 0);
				  y20 : out STD_LOGIC_VECTOR (4 downto 0);
				  y21 : out STD_LOGIC_VECTOR (4 downto 0);
				  y22 : out STD_LOGIC_VECTOR (4 downto 0);
				  y23 : out STD_LOGIC_VECTOR (4 downto 0);
				  y24 : out STD_LOGIC_VECTOR (4 downto 0)
				  );
	end component;
		
		
	component visualizador
		Port ( 
				 CLK : in  STD_LOGIC ;
				 Q_mux : out STD_LOGIC_VECTOR (4 DOWNTO 0);
				 Q_disp : out STD_LOGIC_VECTOR(1 DOWNTO 0);
				 a4 : in STD_LOGIC_VECTOR (4 downto 0);
				 a5 : in STD_LOGIC_VECTOR (4 downto 0);
				 a6 : in STD_LOGIC_VECTOR (4 downto 0);
				 a7 : in STD_LOGIC_VECTOR (4 downto 0);
				 a8 : in STD_LOGIC_VECTOR (4 downto 0);
				 a9 : in STD_LOGIC_VECTOR (4 downto 0);
				 a10 : in STD_LOGIC_VECTOR (4 downto 0);
				 a11 : in STD_LOGIC_VECTOR (4 downto 0);
				 a12 : in STD_LOGIC_VECTOR (4 downto 0);
				 a13 : in STD_LOGIC_VECTOR (4 downto 0);
				 a14 : in STD_LOGIC_VECTOR (4 downto 0);
				 a15 : in STD_LOGIC_VECTOR (4 downto 0);
				 a16 : in STD_LOGIC_VECTOR (4 downto 0);
				 a17 : in STD_LOGIC_VECTOR (4 downto 0);
				 a18 : in STD_LOGIC_VECTOR (4 downto 0);
				 a19 : in STD_LOGIC_VECTOR (4 downto 0);
				 a20 : in STD_LOGIC_VECTOR (4 downto 0);
				 a21 : in STD_LOGIC_VECTOR (4 downto 0);
				 a22 : in STD_LOGIC_VECTOR (4 downto 0);
				 a23 : in STD_LOGIC_VECTOR (4 downto 0);
				 a24 : in STD_LOGIC_VECTOR (4 downto 0);
				 a25 : in STD_LOGIC_VECTOR (4 downto 0);
				 a26 : in STD_LOGIC_VECTOR (4 downto 0);
				 a27 : in STD_LOGIC_VECTOR (4 downto 0);
				 a28 : in STD_LOGIC_VECTOR (4 downto 0);
				 reset : in STD_LOGIC				
			 );
	end component;


	component decodificador 
      Port ( 
				 Q_disp : in  STD_LOGIC_VECTOR (1 downto 0);
             Q_led : out  STD_LOGIC_VECTOR (3 downto 0)
			  );
		end component;	


	component mux_datos 
		Port ( 
				 s0 : in  STD_LOGIC;
				 s1 : in  STD_LOGIC;
				 s2 : in  STD_LOGIC;
				 s3 : in  STD_LOGIC;
				 s4 : in  STD_LOGIC;
             segmentos : out  STD_LOGIC_VECTOR (6 downto 0)
			  );
	 end component;
	 


begin         --begin architecture
	
	
	decodificador1: decodificador port map(
           Q_disp =>signal1,
           Q_led =>ctrl
		  );
			  
	demux1: demux port map( 
           s0 =>dir_memoria(0),
           s1 =>dir_memoria(1),
			  s2 =>dir_memoria(2),
			  s3 =>dir_memoria(3),
			  s4 =>dir_memoria(4),
			  b =>cuenta,
			  y0 =>s_datos0,
			  y1 =>s_datos1,
			  y2 =>s_datos2,
			  y3 =>s_datos3,
			  y4 =>s_datos4,
			  y5 =>s_datos5,
			  y6 =>s_datos6,
			  y7 =>s_datos7,
			  y8 =>s_datos8,			  
			  y9 =>s_datos9,
			  y10 =>s_datos10,
			  y11 =>s_datos11,
			  y12 =>s_datos12,
			  y13 =>s_datos13,
			  y14 =>s_datos14,
			  y15 =>s_datos15,
			  y16 =>s_datos16,
			  y17 =>s_datos17,
			  y18 =>s_datos18,
			  y19 =>s_datos19,
			  y20 =>s_datos20,
			  y21 =>s_datos21,
			  y22 =>s_datos22,
			  y23 =>s_datos23,
			  y24 =>s_datos24
		  );
			  
	mux_datos1: mux_datos port map (
           s0 =>signalMUX(0),
           s1 =>signalMUX(1),
           s2 =>signalMUX(2),
           s3 =>signalMUX(3),
           s4 =>signalMUX(4),
           segmentos =>segmentos
		 );
			  
	visualizador1: visualizador port map(
            CLK=> clk,
			   Q_mux =>signal3,
			   Q_disp=>signal1,
			  	a4=>"11111",--s_datos0,
			   a5=>"11111",--s_datos1,
			   a6=>"11111",--s_datos2,
				a7=>"11111",--s_datos3,
				a8=>s_datos4,--"00000",--s_datos4,
				a9=>s_datos5,--"00001",--s_datos5,
				a10=>s_datos6,---"00010", --s_datos6,
				a11=>s_datos7,--"00011",--s_datos7,
				a12=>s_datos8,--"00100",--s_datos8,
				a13=>s_datos9,---"00101",--s_datos9,
				a14=>s_datos10,--"00110", --s_datos10,
				a15=>s_datos11,--"00111",--s_datos11,
				a16=>s_datos12,--"01000",--s_datos12,
				a17=>s_datos13,--"01001",--s_datos13,
				a18=>s_datos14,--"01010",--s_datos14,
				a19=>s_datos15,--"01011",--s_datos15,
			   a20=>s_datos16,--"01100",--s_datos16,
				a21=>s_datos17,--"01101",--s_datos17,
				a22=>s_datos18,--"01110", --s_datos18,
				a23=>s_datos19,--"01111",--s_datos19,
				a24=>s_datos20,--"10000",--s_datos20,
				a25=>s_datos21,--"10001",--s_datos21,
				a26=>s_datos22,--"10010", --s_datos22,
				a27=>s_datos23,--"10011",--s_datos23,
				a28=>s_datos24,--"10100",--s_datos24,
            reset=> reset
		);
			  


	process(clk)
		begin
			if clk'event and clk='1' then
				
--				if coord=10 then
--					coord<=0;
--				else
--					coord<=coord+1;
--				end if;
--				
				case mode is 
				when '1' =>
						signalMUX<=signal3;
						
				
				when '0' =>
					signalMUX<=signal2;
					signal2<=cuenta;
					if up='0' then
						flanco_up<='0';
					end if;
					if flanco_up='0' and up='1' then
						cuenta<=cuenta+"00001";
						flanco_up<='1';
					end if;
--					IF down'EVENT AND down='1' AND up='0' THEN
--						cuenta<=cuenta-"00001";
--					end if;
					if selected='0' then
						flanco_selected<='0';
						end if;
		--			if coord=5 then	
					if flanco_selected='0' AND selected='1' THEN
						dir_memoria<=dir_memoria+"00001";
						flanco_selected<='1';
						end if;
--							if dir_memoria="11000"then
--								dir_memoria<="00100";
--										
--							else
							
--									dir_memoria<=dir_memoria+"00001";
--								if dir_memoria="00100" and selected='1' then
--								dir_memoria<="00101";
--								end if;
--								if dir_memoria="00101"then
--								dir_memoria<="00110";
--								end if;
--								if dir_memoria="00110"then
--								dir_memoria<="00110";
--								end if;
--								if dir_memoria="00111"then
--								dir_memoria<="01000";
--								end if;
--								if dir_memoria="01000"then
--								dir_memoria<="01001";
--								end if;
--								if dir_memoria="01001"then
--								dir_memoria<="01010";
--								end if;
--								if dir_memoria="01010"then
--								dir_memoria<="01011";
--								end if;
--								if dir_memoria="01011"then
--								dir_memoria<="01100";
--								end if;
--								if dir_memoria="01100"then
--								dir_memoria<="01101";
--								end if;
--								if dir_memoria="01110"then
--								dir_memoria<="01111";
--								end if;
--								if dir_memoria="10000"then
--								dir_memoria<="10001";
--								end if;
--								if dir_memoria="10001"then
--								dir_memoria<="10010";
--								end if;
--								if dir_memoria="10011"then
--								dir_memoria<="10100";
--								end if;
--								if dir_memoria="10101"then
--								dir_memoria<="10110";
--								end if;
--								if dir_memoria="10110"then
--								dir_memoria<="10111";
--								end if;
--								if dir_memoria="10111"then
--								dir_memoria<="11000";
--								end if;
--							end if;
--								flanco_selected<='1';
--						end if;
					
					when others=>
					
					end case;
						
				
				
			--	end if;
			end if;
   end process;

end Behavioral;

