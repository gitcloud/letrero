----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:20:34 12/08/2015 
-- Design Name: 
-- Module Name:    mux_datos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_datos is
    Port ( s0 : in  STD_LOGIC;
           s1 : in  STD_LOGIC;
           s2 : in  STD_LOGIC;
           s3 : in  STD_LOGIC;
           s4 : in  STD_LOGIC;
           segmentos : out  STD_LOGIC_VECTOR (6 downto 0));
end mux_datos;

architecture Behavioral of mux_datos is
signal datos_a: std_logic_vector(31 downto 0);
signal datos_b: std_logic_vector(31 downto 0);
signal datos_c: std_logic_vector(31 downto 0);
signal datos_d: std_logic_vector(31 downto 0);
signal datos_e: std_logic_vector(31 downto 0);
signal datos_f: std_logic_vector(31 downto 0);
signal datos_g: std_logic_vector(31 downto 0);

--utilizacion del modulo mux
component mux
port(
           a : in  STD_LOGIC_VECTOR (31 downto 0);
           s0 : in  STD_LOGIC;
			  s1 : in  STD_LOGIC;
			  s2 : in  STD_LOGIC;
			  s3 : in  STD_LOGIC;
			  s4 : in  STD_LOGIC;
           y : out  STD_LOGIC
);
end component;
begin                                             --todos con logica negada

datos_a<="01110100101100100010100000010011";    
datos_b<="00111100111101011101100001100001";   
datos_c<="00010101000101001101000000000101"; 
datos_d<="00000111011000101000011010010011";  
datos_e<="01001010000000000000001010111011";  
datos_f<="00100100111011000010000010001111";  
datos_g<="10100000000111000001000010000011";   

segmento_a: mux port map(
       a=> datos_a,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(6)
);	
segmento_b: mux port map(
       a=> datos_b,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(5)
);	
segmento_c: mux port map(
       a=> datos_c,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(4)
);	
segmento_d: mux port map(
       a=> datos_d,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(3)
);	
segmento_e: mux port map(
       a=> datos_e,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(2)
);		
segmento_f: mux port map(
       a=> datos_f,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(1)
);	
segmento_g: mux port map(
       a=> datos_g,
       s0=>s0,
       s1=>s1,	 
       s2=>s2,		 
       s3=>s3,		 
       s4=>s4,
		 y=>segmentos(0)
);	 


end Behavioral;

