--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:55:03 01/25/2016
-- Design Name:   
-- Module Name:   C:/Users/sed/Desktop/rotuloV58/desplazamiento_tb.vhd
-- Project Name:  rotulo_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: desplazamiento
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY desplazamiento_tb IS
END desplazamiento_tb;
 
ARCHITECTURE behavior OF desplazamiento_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT desplazamiento
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         e0 : IN  std_logic_vector(4 downto 0);
         e1 : IN  std_logic_vector(4 downto 0);
         e2 : IN  std_logic_vector(4 downto 0);
         e3 : IN  std_logic_vector(4 downto 0);
         e4 : IN  std_logic_vector(4 downto 0);
         e5 : IN  std_logic_vector(4 downto 0);
         e6 : IN  std_logic_vector(4 downto 0);
         e7 : IN  std_logic_vector(4 downto 0);
         e8 : IN  std_logic_vector(4 downto 0);
         e9 : IN  std_logic_vector(4 downto 0);
         e10 : IN  std_logic_vector(4 downto 0);
         e11 : IN  std_logic_vector(4 downto 0);
         e12 : IN  std_logic_vector(4 downto 0);
         e13 : IN  std_logic_vector(4 downto 0);
         e14 : IN  std_logic_vector(4 downto 0);
         e15 : IN  std_logic_vector(4 downto 0);
         e16 : IN  std_logic_vector(4 downto 0);
         e17 : IN  std_logic_vector(4 downto 0);
         e18 : IN  std_logic_vector(4 downto 0);
         e19 : IN  std_logic_vector(4 downto 0);
         e20 : IN  std_logic_vector(4 downto 0);
         e21 : IN  std_logic_vector(4 downto 0);
         e22 : IN  std_logic_vector(4 downto 0);
         e23 : IN  std_logic_vector(4 downto 0);
         e24 : IN  std_logic_vector(4 downto 0);
         y0 : OUT  std_logic_vector(4 downto 0);
         y1 : OUT  std_logic_vector(4 downto 0);
         y2 : OUT  std_logic_vector(4 downto 0);
         y3 : OUT  std_logic_vector(4 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal e0 : std_logic_vector(4 downto 0) := (others => '0');
   signal e1 : std_logic_vector(4 downto 0) := (others => '0');
   signal e2 : std_logic_vector(4 downto 0) := (others => '0');
   signal e3 : std_logic_vector(4 downto 0) := (others => '0');
   signal e4 : std_logic_vector(4 downto 0) := (others => '0');
   signal e5 : std_logic_vector(4 downto 0) := (others => '0');
   signal e6 : std_logic_vector(4 downto 0) := (others => '0');
   signal e7 : std_logic_vector(4 downto 0) := (others => '0');
   signal e8 : std_logic_vector(4 downto 0) := (others => '0');
   signal e9 : std_logic_vector(4 downto 0) := (others => '0');
   signal e10 : std_logic_vector(4 downto 0) := (others => '0');
   signal e11 : std_logic_vector(4 downto 0) := (others => '0');
   signal e12 : std_logic_vector(4 downto 0) := (others => '0');
   signal e13 : std_logic_vector(4 downto 0) := (others => '0');
   signal e14 : std_logic_vector(4 downto 0) := (others => '0');
   signal e15 : std_logic_vector(4 downto 0) := (others => '0');
   signal e16 : std_logic_vector(4 downto 0) := (others => '0');
   signal e17 : std_logic_vector(4 downto 0) := (others => '0');
   signal e18 : std_logic_vector(4 downto 0) := (others => '0');
   signal e19 : std_logic_vector(4 downto 0) := (others => '0');
   signal e20 : std_logic_vector(4 downto 0) := (others => '0');
   signal e21 : std_logic_vector(4 downto 0) := (others => '0');
   signal e22 : std_logic_vector(4 downto 0) := (others => '0');
   signal e23 : std_logic_vector(4 downto 0) := (others => '0');
   signal e24 : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal y0 : std_logic_vector(4 downto 0);
   signal y1 : std_logic_vector(4 downto 0);
   signal y2 : std_logic_vector(4 downto 0);
   signal y3 : std_logic_vector(4 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: desplazamiento PORT MAP (
          clk => clk,
          reset => reset,
          e0 => e0,
          e1 => e1,
          e2 => e2,
          e3 => e3,
          e4 => e4,
          e5 => e5,
          e6 => e6,
          e7 => e7,
          e8 => e8,
          e9 => e9,
          e10 => e10,
          e11 => e11,
          e12 => e12,
          e13 => e13,
          e14 => e14,
          e15 => e15,
          e16 => e16,
          e17 => e17,
          e18 => e18,
          e19 => e19,
          e20 => e20,
          e21 => e21,
          e22 => e22,
          e23 => e23,
          e24 => e24,
          y0 => y0,
          y1 => y1,
          y2 => y2,
          y3 => y3
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		e0 <= "11111";
		e1 <= "11110";
		e2 <= "11101";
		e3 <= "11100";
		e4 <= "11011";
		e5 <= "11010";
		e6 <= "11001";
		e7 <= "11000";
		e8 <= "10111";
		e9 <= "10110";
		e10 <= "10101";
		e11 <= "10100";
		e12 <= "10011";
		e13 <= "10010";
		e14 <= "10001";
		e15 <= "10000";
		e16 <= "01111";
		e17 <= "01110";
		e18 <= "01101";
		e19 <= "01100";
		e20 <= "01011";
		e21 <= "01010";
		e22 <= "01001";
		e23 <= "01000";
		e24 <= "00111";
      wait for 1000 ns;	

      --wait for clk_period*10;

      -- insert stimulus here 

      --wait;
		assert false
		report "Fin simulacion"
		severity failure;
   end process;

END;
