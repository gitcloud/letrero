--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   20:34:05 01/20/2016
-- Design Name:   
-- Module Name:   F:/Lab.SED/rotulo_movil/reloj_1hz_tb.vhd
-- Project Name:  rotulo_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: clk_divider
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY reloj_1hz_tb IS
END reloj_1hz_tb;
 
ARCHITECTURE behavior OF reloj_1hz_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT clk_divider
    PORT(
         clk_in : IN  std_logic;
         reset : IN  std_logic;
         clk_1hz : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk_in : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal clk_1hz : std_logic;

   -- Clock period definitions
   constant k : time := 20 ns;
--   constant clk_1hz_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: clk_divider PORT MAP (
          clk_in => clk_in,
          reset => reset,
          clk_1hz => clk_1hz
        );

   -- Clock process definitions
   clk_in_process :process
   begin
		clk_in <= '0';
		wait for k/2;
		clk_in <= '1';
		wait for k/2;
   end process;
 
--   clk_1hz_process :process
--   begin
--		clk_1hz <= '0';
--		wait for clk_1hz_period/2;
--		clk_1hz <= '1';
--		wait for clk_1hz_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 10 ns;	

      

      -- insert stimulus here 

      wait;
   end process;

END;
