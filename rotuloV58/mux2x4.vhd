----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:09:30 01/12/2016 
-- Design Name: 
-- Module Name:    mux2x4 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux2x4 is
PORT(  a0:in STD_LOGIC_VECTOR (4 downto 0);
       a1:in STD_LOGIC_VECTOR (4 downto 0);
		 a2:in STD_LOGIC_VECTOR (4 downto 0);
		 a3:in STD_LOGIC_VECTOR (4 downto 0);
		 Q_muxx:out STD_LOGIC_VECTOR (4 downto 0);
		 s0:in STD_LOGIC;
		 s1:in STD_LOGIC
		 );
		
end mux2x4;

architecture Behavioral of mux2x4 is
	signal r: STD_LOGIC_VECTOR(1 downto 0);
begin
	r<=s1&s0;
	with r select
		Q_muxx<= a0 when "00",
					a3 when "01",
					a2 when "10",
					a1 when others;

end Behavioral;

