----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:57:54 12/06/2015 
-- Design Name: 
-- Module Name:    mux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux is
    Port ( a : in  STD_LOGIC_VECTOR (31 downto 0);
           s0 : in  STD_LOGIC;
			  s1 : in  STD_LOGIC;
			  s2 : in  STD_LOGIC;
			  s3 : in  STD_LOGIC;
			  s4 : in  STD_LOGIC;
           y : out  STD_LOGIC);
end mux;

architecture Behavioral of mux is
signal r: STD_LOGIC_VECTOR (4 DOWNTO 0);
begin
r<= s4 & s3 & s2 & s1 & s0;
with r select
y<=    a(0) when "00000",
       a(1) when "00001",
		 a(2) when "00010",
		 a(3) when "00011",
       a(4) when "00100",
       a(5) when "00101",
		 a(6) when "00110",
		 a(7) when "00111",
       a(8) when "01000",
       a(9) when "01001",
		 a(10) when "01010",
		 a(11) when "01011",
       a(12) when "01100",
       a(13) when "01101",
		 a(14) when "01110",
		 a(15) when "01111",
       a(16) when "10000",
       a(17) when "10001",
		 a(18) when "10010",
		 a(19) when "10011",
       a(20) when "10100",
       a(21) when "10101",
		 a(22) when "10110",
		 a(23) when "10111",
       a(24) when "11000",
       a(25) when "11001",
		 a(26) when "11010",
		 a(27) when "11011",
       a(28) when "11100",
       a(29) when "11101",
		 a(30) when "11110",
		 a(31) when others;





end Behavioral;

