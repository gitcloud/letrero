----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:37:09 01/13/2016 
-- Design Name: 
-- Module Name:    demux - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity demux is
    Port ( s0 : in  STD_LOGIC;
           s1 : in  STD_LOGIC;
			  s2 : in  STD_LOGIC;
			  s3 : in  STD_LOGIC;
			  s4 : in  STD_LOGIC;
           b : in  STD_LOGIC_VECTOR (4 downto 0);
			  y0 : out STD_LOGIC_VECTOR (4 downto 0);
			  y1 : out STD_LOGIC_VECTOR (4 downto 0);
			  y2 : out STD_LOGIC_VECTOR (4 downto 0);
			  y3 : out STD_LOGIC_VECTOR (4 downto 0);
			  y4 : out STD_LOGIC_VECTOR (4 downto 0);
			  y5 : out STD_LOGIC_VECTOR (4 downto 0);
			  y6 : out STD_LOGIC_VECTOR (4 downto 0);
			  y7 : out STD_LOGIC_VECTOR (4 downto 0);
			  y8 : out STD_LOGIC_VECTOR (4 downto 0);
			  y9 : out STD_LOGIC_VECTOR (4 downto 0);
			  y10 : out STD_LOGIC_VECTOR (4 downto 0);
			  y11 : out STD_LOGIC_VECTOR (4 downto 0);
			  y12 : out STD_LOGIC_VECTOR (4 downto 0);
			  y13 : out STD_LOGIC_VECTOR (4 downto 0);
			  y14 : out STD_LOGIC_VECTOR (4 downto 0);
			  y15 : out STD_LOGIC_VECTOR (4 downto 0);
			  y16 : out STD_LOGIC_VECTOR (4 downto 0);
			  y17 : out STD_LOGIC_VECTOR (4 downto 0);
			  y18 : out STD_LOGIC_VECTOR (4 downto 0);
			  y19 : out STD_LOGIC_VECTOR (4 downto 0);
			  y20 : out STD_LOGIC_VECTOR (4 downto 0);
			  y21 : out STD_LOGIC_VECTOR (4 downto 0);
			  y22 : out STD_LOGIC_VECTOR (4 downto 0);
			  y23 : out STD_LOGIC_VECTOR (4 downto 0);
			  y24 : out STD_LOGIC_VECTOR (4 downto 0)
			  );
		  end demux;

architecture Behavioral of demux is

	signal r: std_logic_vector (4 downto 0);
	

begin
	r<=s4&s3&s2&s1&s0;
	y0<="11111";
	y1<="11111";
	y2<="11111";
	y3<="11111";
	y4<= b when r="00100";
	y5<= b when r="00101";
	y6<= b when r="00110";
	y7<= b when r="00111";
	y8<= b when r="01000";
	y9<= b when r="01001";
	y10<= b when r="01010";
	y11<= b when r="01011";
	y12<= b when r="01100";
	y13<= b when r="01101";
	y14<= b when r="01110";
	y15<= b when r="01111";
	y16<= b when r="10000";
	y17<= b when r="10001";
	y18<= b when r="10010";
	y19<= b when r="10011";
	y20<= b when r="10100";
	y21<= b when r="10101";
	y22<= b when r="10110";
	y23<= b when r="10111";
	y24<= b when r="11000";
	
		


end Behavioral;

