----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:02:18 12/22/2015 
-- Design Name: 
-- Module Name:    letra_ffd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity letra_ffd is

    Port ( CLK : in  STD_LOGIC;
           letra : in  STD_LOGIC_VECTOR (4 downto 0);
           --control_l : in  STD_LOGIC_VECTOR (1 downto 0);
           Q_mux : out  STD_LOGIC_VECTOR (4 downto 0)
--           Q_disp : out  STD_LOGIC_VECTOR (1 downto 0)
			  );
end letra_ffd;

architecture Behavioral of letra_ffd is

component ff_d
		Port ( D : in  STD_LOGIC;
             CLK : in  STD_LOGIC;
             Q : out  STD_LOGIC
			  );
	
	end component;
	
begin
    letra0: ff_d port map(
       D=> letra(0),
       CLK=>CLK,
       Q=>Q_mux(0)	 
       
);	
letra1: ff_d port map(
       D=> letra(1),
       CLK=>CLK,
       Q=>Q_mux(1)	 
       
);	
letra2: ff_d port map(
       D=> letra(2),
       CLK=>CLK,
       Q=>Q_mux(2)	 
       
);	
letra3: ff_d port map(
       D=> letra(3),
       CLK=>CLK,
       Q=>Q_mux(3)	 
       
);	
letra4: ff_d port map(
       D=> letra(4),
       CLK=>CLK,
       Q=>Q_mux(4)	 
       
);	
--control0: ff_d port map(
      -- D=> control_l(0),
      -- CLK=>CLK,
      -- Q=>Q_disp(0)	 
       
--);	
--control1: ff_d port map(
       --D=> control_l(1),
       --CLK=>CLK,
      -- Q=>Q_disp(1)	 
       
--);	
end Behavioral;

