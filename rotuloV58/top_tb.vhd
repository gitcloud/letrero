--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:01:14 01/25/2016
-- Design Name:   
-- Module Name:   C:/Users/sed/Desktop/rotuloV58/top_tb.vhd
-- Project Name:  rotulo_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY top_tb IS
END top_tb;
 
ARCHITECTURE behavior OF top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT top
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         selected : IN  std_logic;
         up : IN  std_logic;
         down : IN  std_logic;
         mode : IN  std_logic;
         segmentos : OUT  std_logic_vector(6 downto 0);
         ctrl : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal selected : std_logic := '0';
   signal up : std_logic := '0';
   signal down : std_logic := '0';
   signal mode : std_logic := '0';

 	--Outputs
   signal segmentos : std_logic_vector(6 downto 0);
   signal ctrl : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant k: time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top PORT MAP (
          clk => clk,
          reset => reset,
          selected => selected,
          up => up,
          down => down,
          mode => mode,
          segmentos => segmentos,
          ctrl => ctrl
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for k/2;
		clk <= '1';
		wait for k/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 n
	   WAIT FOR 11ns;
		reset<='1';
		WAIT FOR 11ns;
		reset<='0';
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
wait for 11ns;
selected<='1';
wait for 25ns;
selected<='0';
wait for 5ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
wait for 11ns;
selected<='1';
wait for 20ns;
selected<='0';
wait for 5ns;
      up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
wait for 11ns;
selected<='1';
wait for 15ns;
selected<='0';
wait for 5ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
wait for 11ns;
selected<='1';
wait for 35ns;
selected<='0';
wait for 5ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
		wait for 11ns;
		up<='1';
		wait for 11ns;
		up<='0';
wait for 11ns;
selected<='1';
wait for 25ns;
selected<='0';
mode<='1';
      -- insert stimulus here 

      wait;
   end process;

END;
