--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:43:44 01/20/2016
-- Design Name:   
-- Module Name:   F:/Lab.SED/rotulo_movil/visualizador_tb.vhd
-- Project Name:  rotulo_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: visualizador
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY visualizador_tb IS
END visualizador_tb;
 
ARCHITECTURE behavior OF visualizador_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT visualizador
    PORT(
         CLK : IN  std_logic;
         reset : IN  std_logic;
         Q_mux : OUT  std_logic_vector(4 downto 0);
         Q_disp : OUT  std_logic_vector(1 downto 0);
         a4 : IN  std_logic_vector(4 downto 0);
         a5 : IN  std_logic_vector(4 downto 0);
         a6 : IN  std_logic_vector(4 downto 0);
         a7 : IN  std_logic_vector(4 downto 0);
         a8 : IN  std_logic_vector(4 downto 0);
         a9 : IN  std_logic_vector(4 downto 0);
         a10 : IN  std_logic_vector(4 downto 0);
         a11 : IN  std_logic_vector(4 downto 0);
         a12 : IN  std_logic_vector(4 downto 0);
         a13 : IN  std_logic_vector(4 downto 0);
         a14 : IN  std_logic_vector(4 downto 0);
         a15 : IN  std_logic_vector(4 downto 0);
         a16 : IN  std_logic_vector(4 downto 0);
         a17 : IN  std_logic_vector(4 downto 0);
         a18 : IN  std_logic_vector(4 downto 0);
         a19 : IN  std_logic_vector(4 downto 0);
         a20 : IN  std_logic_vector(4 downto 0);
         a21 : IN  std_logic_vector(4 downto 0);
         a22 : IN  std_logic_vector(4 downto 0);
         a23 : IN  std_logic_vector(4 downto 0);
         a24 : IN  std_logic_vector(4 downto 0);
         a25 : IN  std_logic_vector(4 downto 0);
         a26 : IN  std_logic_vector(4 downto 0);
         a27 : IN  std_logic_vector(4 downto 0);
         a28 : IN  std_logic_vector(4 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal CLK : std_logic := '1';
   signal reset : std_logic := '0';
   signal a4 : std_logic_vector(4 downto 0) := (others => '0');
   signal a5 : std_logic_vector(4 downto 0) := (others => '0');
   signal a6 : std_logic_vector(4 downto 0) := (others => '0');
   signal a7 : std_logic_vector(4 downto 0) := (others => '0');
   signal a8 : std_logic_vector(4 downto 0) := (others => '0');
   signal a9 : std_logic_vector(4 downto 0) := (others => '0');
   signal a10 : std_logic_vector(4 downto 0) := (others => '0');
   signal a11 : std_logic_vector(4 downto 0) := (others => '0');
   signal a12 : std_logic_vector(4 downto 0) := (others => '0');
   signal a13 : std_logic_vector(4 downto 0) := (others => '0');
   signal a14 : std_logic_vector(4 downto 0) := (others => '0');
   signal a15 : std_logic_vector(4 downto 0) := (others => '0');
   signal a16 : std_logic_vector(4 downto 0) := (others => '0');
   signal a17 : std_logic_vector(4 downto 0) := (others => '0');
   signal a18 : std_logic_vector(4 downto 0) := (others => '0');
   signal a19 : std_logic_vector(4 downto 0) := (others => '0');
   signal a20 : std_logic_vector(4 downto 0) := (others => '0');
   signal a21 : std_logic_vector(4 downto 0) := (others => '0');
   signal a22 : std_logic_vector(4 downto 0) := (others => '0');
   signal a23 : std_logic_vector(4 downto 0) := (others => '0');
   signal a24 : std_logic_vector(4 downto 0) := (others => '0');
   signal a25 : std_logic_vector(4 downto 0) := (others => '0');
   signal a26 : std_logic_vector(4 downto 0) := (others => '0');
   signal a27 : std_logic_vector(4 downto 0) := (others => '0');
   signal a28 : std_logic_vector(4 downto 0) := (others => '0');

 	--Outputs
   signal Q_mux : std_logic_vector(4 downto 0);
   signal Q_disp : std_logic_vector(1 downto 0);

   -- Clock period definitions
   constant k : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: visualizador PORT MAP (
          CLK => CLK,
          reset => reset,
          Q_mux => Q_mux,
          Q_disp => Q_disp,
          a4 => a4,
          a5 => a5,
          a6 => a6,
          a7 => a7,
          a8 => a8,
          a9 => a9,
          a10 => a10,
          a11 => a11,
          a12 => a12,
          a13 => a13,
          a14 => a14,
          a15 => a15,
          a16 => a16,
          a17 => a17,
          a18 => a18,
          a19 => a19,
          a20 => a20,
          a21 => a21,
          a22 => a22,
          a23 => a23,
          a24 => a24,
          a25 => a25,
          a26 => a26,
          a27 => a27,
          a28 => a28
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for k/2;
		CLK <= '1';
		wait for k/2;
   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin	
			 wait for 28ns;
			 a4 <= "10100";
          a5 <= "00111";
          a6 <= "10011";
          a7 <= "01100";
          a8 <= "10100";
          a9 <= "00111";
          a10 <= "10011";
          a11 <= "01100";
          a12 <= "00001";
          a13 <= "11010";
			 a14 <= "10100";
          a15 <= "00111";
          a16 <= "10011";
          a17 <= "01100";
          a18 <= "00001";
          a19 <= "11010";
			 a20 <= "10100";
          a21 <= "00111";
          a22 <= "10011";
          a23 <= "01100";
          a24 <= "00001";
          a25 <= "11010";
			 a26 <= "10100";
          a27 <= "00111";
          a28 <= "10011";


      wait;
   end process;

END;
