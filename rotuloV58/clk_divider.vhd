----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:06:34 01/13/2016 
-- Design Name: 
-- Module Name:    clk_divider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.STD_LOGIC_arith.ALL;
use ieee.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clk_divider is

    Port ( reset : in  STD_LOGIC;
           clk_in : in  STD_LOGIC;
           clk_1hz : out  STD_LOGIC;
			  clk_400hz : out  STD_LOGIC
			  );
end clk_divider;


architecture Behavioral of clk_divider is
signal temp: std_logic;
signal counter: integer range 0 to 5999024;
signal temp2: std_logic;
signal counter2: integer range 0 to 69024;
begin

    clk_1hz <=temp;
    process(reset, clk_in)
    begin
        if rising_edge(clk_in) then
           if reset='1' then    
                temp <= '0';
                counter <= 0;

            else 
                if counter = 5999024 then 
                    temp <= not (temp);
                   counter <= 0;

                else
                    counter <= counter + 1;
                end if;
            end if;
        end if;
    end process;
	 
	 clk_400hz <=temp2;
    process(reset, clk_in)
    begin
        if rising_edge(clk_in) then
           if reset='1' then    
                temp2 <= '0';
                counter2 <= 0;

            else 
                if counter2 = 69024 then 
                    temp2 <= not (temp2);
                   counter2 <= 0;

                else
                    counter2 <= counter2 + 1;
                end if;
            end if;
        end if;
    end process;
end Behavioral;

--entity clk_divider is
----	generic (prescal: positive := 40);
--	port (
--		clk_in : in  std_logic;
--		reset  : in  std_logic;
--		clk_1hz: out std_logic
--	);
--end clk_divider;
--
--architecture behavioral of clk_divider is
----	signal prescaler: integer range 0 to prescal;
--signal clock : std_logic;
--signal c_flancos: std_logic_vector(25 downto 0):="00000000000000000000000000";
--begin
--	process (clk_in, reset)
--		begin 
--		if clk_in'event and clk_in='1' then
--			if reset = '1' then
--			clk_1hz <= clo;
----			prescaler <= 0;
--			end if;
--			if reset='0' then
--				
--					c_flancos<=c_flancos+"00000000000000000000000001";
--					
--				if c_flancos="10111110101111000010000000" then
--					clk_1hz<='1';
--					c_flancos<="00000000000000000000000000";
--				else 
--					clk_1hz<=not(clk_1hz);
--				end if;
----				if c_flancos<"10111110101111000010000000" then
----					clk_1hz<='0';
----				end if;
--			end if;
--		end if;
--		
----			if prescaler = prescal then --Tenemos periodo de 50Mhz y lo queremos pasar a 1hz (cambia contador cada 1s).
----				prescaler <= 0;
----				clk_1hz_i <= not clk_1hz_i;
----			else
----				prescaler <= prescaler + 1;
----			end if;
----		end if;
--	end process;
----
----	clk_1hz <= clk_1hz_i;
--
--end Behavioral;
