----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:25:48 12/22/2015 
-- Design Name: 
-- Module Name:    visualizador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.STD_LOGIC_arith.ALL;
use ieee.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity visualizador is
	
    Port ( CLK : in  STD_LOGIC;
			  reset : in STD_LOGIC;
			  Q_mux : out STD_LOGIC_VECTOR (4 DOWNTO 0);
			  Q_disp : out STD_LOGIC_VECTOR(1 DOWNTO 0);
			  a4:in STD_LOGIC_VECTOR (4 downto 0);
			  a5:in STD_LOGIC_VECTOR (4 downto 0);
			  a6:in STD_LOGIC_VECTOR (4 downto 0);
			  a7:in STD_LOGIC_VECTOR (4 downto 0);
			  a8:in STD_LOGIC_VECTOR (4 downto 0);
		     a9:in STD_LOGIC_VECTOR (4 downto 0);
		     a10:in STD_LOGIC_VECTOR (4 downto 0);
			  a11:in STD_LOGIC_VECTOR (4 downto 0);
			  a12:in STD_LOGIC_VECTOR (4 downto 0);
			  a13:in STD_LOGIC_VECTOR (4 downto 0);
			  a14:in STD_LOGIC_VECTOR (4 downto 0);
			  a15:in STD_LOGIC_VECTOR (4 downto 0);
			  a16:in STD_LOGIC_VECTOR (4 downto 0);
			  a17:in STD_LOGIC_VECTOR (4 downto 0);
			  a18:in STD_LOGIC_VECTOR (4 downto 0);
			  a19:in STD_LOGIC_VECTOR (4 downto 0);
			  a20:in STD_LOGIC_VECTOR (4 downto 0);
			  a21:in STD_LOGIC_VECTOR (4 downto 0);
			  a22:in STD_LOGIC_VECTOR (4 downto 0);
			  a23:in STD_LOGIC_VECTOR (4 downto 0);
			  a24:in STD_LOGIC_VECTOR (4 downto 0);
			  a25:in STD_LOGIC_VECTOR (4 downto 0);
			  a26:in STD_LOGIC_VECTOR (4 downto 0);
			  a27:in STD_LOGIC_VECTOR (4 downto 0);
			  a28:in STD_LOGIC_VECTOR (4 downto 0)		 
		 );
end visualizador;




architecture Behavioral of visualizador is
    
   
	component clk_divider
		port (
				clk_in : in  std_logic;
				reset  : in  std_logic;
				clk_1hz: out std_logic;
			 clk_400hz : out  STD_LOGIC
			);
	end component;
		
	component letra_ffd
	
		Port( CLK : in  STD_LOGIC;
            letra : in  STD_LOGIC_VECTOR (4 downto 0);
         --  control_l : in  STD_LOGIC_VECTOR (1 downto 0);
			   Q_mux : out  STD_LOGIC_VECTOR (4 downto 0)
--           Q_disp : out  STD_LOGIC_VECTOR (1 downto 0)
			  );
	end component;
  
  component mux2x4
	 Port(
			a0:in STD_LOGIC_VECTOR (4 downto 0);
			a1:in STD_LOGIC_VECTOR (4 downto 0);
			a2:in STD_LOGIC_VECTOR (4 downto 0);
			a3:in STD_LOGIC_VECTOR (4 downto 0);
			Q_muxx:out STD_LOGIC_VECTOR (4 downto 0);
			s0:in STD_LOGIC;
			s1:in STD_LOGIC
	 );
	end component;
	
	component desplazamiento
		Port(
			  clk : in  STD_LOGIC;
			  reset : in  STD_LOGIC;
           e0 : in  STD_LOGIC_VECTOR (4 downto 0);
           e1 : in  STD_LOGIC_VECTOR (4 downto 0);
           e2 : in  STD_LOGIC_VECTOR (4 downto 0);
           e3 : in  STD_LOGIC_VECTOR (4 downto 0);
           e4 : in  STD_LOGIC_VECTOR (4 downto 0);
           e5 : in  STD_LOGIC_VECTOR (4 downto 0);
           e6 : in  STD_LOGIC_VECTOR (4 downto 0);
           e7 : in  STD_LOGIC_VECTOR (4 downto 0);
           e8 : in  STD_LOGIC_VECTOR (4 downto 0);
           e9 : in  STD_LOGIC_VECTOR (4 downto 0);
           e10 : in  STD_LOGIC_VECTOR (4 downto 0);
           e11 : in  STD_LOGIC_VECTOR (4 downto 0);
           e12 : in  STD_LOGIC_VECTOR (4 downto 0);
           e13 : in  STD_LOGIC_VECTOR (4 downto 0);
           e14 : in  STD_LOGIC_VECTOR (4 downto 0);
           e15 : in  STD_LOGIC_VECTOR (4 downto 0);
           e16 : in  STD_LOGIC_VECTOR (4 downto 0);
           e17 : in  STD_LOGIC_VECTOR (4 downto 0);
           e18 : in  STD_LOGIC_VECTOR (4 downto 0);
           e19 : in  STD_LOGIC_VECTOR (4 downto 0);
           e20 : in  STD_LOGIC_VECTOR (4 downto 0);
           e21 : in  STD_LOGIC_VECTOR (4 downto 0);
           e22 : in  STD_LOGIC_VECTOR (4 downto 0);
           e23 : in  STD_LOGIC_VECTOR (4 downto 0);
           e24 : in  STD_LOGIC_VECTOR (4 downto 0);
           y0 : out  STD_LOGIC_VECTOR (4 downto 0);
           y1 : out  STD_LOGIC_VECTOR (4 downto 0);
           y2 : out  STD_LOGIC_VECTOR (4 downto 0);
           y3 : out  STD_LOGIC_VECTOR (4 downto 0)
			);
			end component;
	
signal led: STD_LOGIC_VECTOR (1 downto 0):="00";  --esta se�al indicar� en que display se va a utilizar, sirve para refresco
signal i0: STD_LOGIC_VECTOR (4 downto 0);
signal i1: STD_LOGIC_VECTOR (4 downto 0);
signal i2: STD_LOGIC_VECTOR (4 downto 0);
signal i3: STD_LOGIC_VECTOR (4 downto 0);
signal i4: STD_LOGIC_VECTOR (4 downto 0);
signal i5: STD_LOGIC_VECTOR (4 downto 0);
signal i6: STD_LOGIC_VECTOR (4 downto 0);
signal i7: STD_LOGIC_VECTOR (4 downto 0);
signal i8: STD_LOGIC_VECTOR (4 downto 0);
signal i9: STD_LOGIC_VECTOR (4 downto 0);
signal i10: STD_LOGIC_VECTOR (4 downto 0);
signal i11: STD_LOGIC_VECTOR (4 downto 0);
signal i12: STD_LOGIC_VECTOR (4 downto 0);
signal i13: STD_LOGIC_VECTOR (4 downto 0);
signal i14: STD_LOGIC_VECTOR (4 downto 0);
signal i15: STD_LOGIC_VECTOR (4 downto 0);
signal i16: STD_LOGIC_VECTOR (4 downto 0);
signal i17: STD_LOGIC_VECTOR (4 downto 0);
signal i18: STD_LOGIC_VECTOR (4 downto 0);
signal i19: STD_LOGIC_VECTOR (4 downto 0);
signal i20: STD_LOGIC_VECTOR (4 downto 0);
signal i21: STD_LOGIC_VECTOR (4 downto 0);
signal i22: STD_LOGIC_VECTOR (4 downto 0);
signal i23: STD_LOGIC_VECTOR (4 downto 0);
signal i24: STD_LOGIC_VECTOR (4 downto 0);
signal y0: STD_LOGIC_VECTOR (4 downto 0);
signal y1: STD_LOGIC_VECTOR (4 downto 0);
signal y2: STD_LOGIC_VECTOR (4 downto 0);
signal y3: STD_LOGIC_VECTOR (4 downto 0);

signal clk_desp: STD_LOGIC;
signal clk_desp2: STD_LOGIC;
--signal clk_desp3: STD_LOGIC;

begin
    
	 
	 
	reloj_1hz: clk_divider port map(
				clk_in =>clk,
				reset =>reset,
				clk_1hz=> clk_desp,
				clk_400hz=>clk_desp2
			);
	
	multiplexor: mux2x4 port map(
            Q_muxx=>Q_mux,
				a0=>y0,
				a1=>y1,
				a2=>y2,
				a3=>y3,
            s0=>led(0),
				s1=>led(1)
				
			);
			
	desplazamiento1: desplazamiento port map(
			  reset=>reset,
			  clk=>clk_desp,
           e0=>i0,--"00001",
           e1=>i1,--"00001",
           e2=>i2,--"00001",
           e3=>i3,--"00100",
           e4=>i4,--"00101",
           e5=>i5,--"00110",
           e6=>i6,--"00111",--i6,
           e7=>i7,--"01000",--i7,
           e8=>i8,--"01001",--i8,
           e9=>i9,--"01010",--i9,
           e10=>i10,--"01011",--i10,
           e11=>i11,--"01100",--i11,
           e12=>i12,--"01101",--i12,
           e13=>i13,--"01110",--i13,
           e14=>i14,--"01111",--i14,
           e15=>i15,--"10000",--i15,
           e16=>i16,--"10001",--i16,
           e17=>i17,--"10010",--i17,
           e18=>i18,--"10011",--i18,
           e19=>i19,--"10100",--i19,
           e20=>i20,--"10101",--i20,
           e21=>i21,--"10110",--i21,
           e22=>i22,--"10111",--i22,
           e23=>i23,--"11000",
           e24=>i24,--"00000",
           y0=>y0,
           y1=>y1,
           y2=>y2,
           y3=>y3
			  );

	letra_ffd0: letra_ffd port map(
           CLK=>CLK,
			  letra=>a4,
			  Q_mux=>i0
--			 Q_disp=>Q_disp
			 );
			 
	letra_ffd1: letra_ffd port map(
           CLK=>CLK,
			  letra=>a5,
			  Q_mux=>i1
--		     Q_disp=>Q_disp
			  );
			  
	letra_ffd2: letra_ffd port map(
           CLK=>CLK,
			  letra=>a6,
			  Q_mux=>i2
--			  Q_disp=>Q_disp
			  );
	
	letra_ffd3: letra_ffd port map(
           CLK=>CLK,
			   letra=>a7,
			  Q_mux=>i3
--			 Q_disp=>Q_disp
			  );

	letra_ffd4: letra_ffd port map(
           CLK=>CLK,
			   letra=>a8,
			  Q_mux=>i4
--			 Q_disp=>Q_disp
			  );

	letra_ffd5: letra_ffd port map(
           CLK=>CLK,
			   letra=>a9,
			  Q_mux=>i5
			-- Q_disp=>Q_disp
			  );

	letra_ffd6: letra_ffd port map(
           CLK=>CLK,
			  letra=>a10,
			  Q_mux=>i6
			-- Q_disp=>Q_disp
			 );

	letra_ffd7: letra_ffd port map(
           CLK=>CLK,
			  letra=>a11,
			  Q_mux=>i7
		   --  Q_disp=>Q_disp
			  );

	letra_ffd8: letra_ffd port map(
           CLK=>CLK,
			  letra=>a12,
			  Q_mux=>i8
			--  Q_disp=>Q_disp
			  );

	letra_ffd9: letra_ffd port map(
           CLK=>CLK,
			   letra=>a13,
			  Q_mux=>i9
			-- Q_disp=>Q_disp
			  );

	letra_ffd10: letra_ffd port map(
           CLK=>CLK,
			   letra=>a14,
			  Q_mux=>i10
			-- Q_disp=>Q_disp
			  );

	letra_ffd11: letra_ffd port map(
           CLK=>CLK,
			   letra=>a15,
			  Q_mux=>i11
			-- Q_disp=>Q_disp
			  );

	letra_ffd12: letra_ffd port map(
           CLK=>CLK,
			  letra=>a16,
			  Q_mux=>i12
			-- Q_disp=>Q_disp
			 );

	letra_ffd13: letra_ffd port map(
           CLK=>CLK,
			  letra=>a17,
			  Q_mux=>i13
		   --  Q_disp=>Q_disp
			  );

	letra_ffd14: letra_ffd port map(
           CLK=>CLK,
			  letra=>a18,
			  Q_mux=>i14
			--  Q_disp=>Q_disp
			  );

	letra_ffd15: letra_ffd port map(
           CLK=>CLK,
			   letra=>a19,
			  Q_mux=>i15
			-- Q_disp=>Q_disp
			  );

	letra_ffd16: letra_ffd port map(
           CLK=>CLK,
			   letra=>a20,
			  Q_mux=>i16
			-- Q_disp=>Q_disp
			  );

	letra_ffd17: letra_ffd port map(
           CLK=>CLK,
			   letra=>a21,
			  Q_mux=>i17
			-- Q_disp=>Q_disp
			  );
			  
	letra_ffd18: letra_ffd port map(
           CLK=>CLK,
			   letra=>a22,
			  Q_mux=>i18
			-- Q_disp=>Q_disp
			  );
	
	letra_ffd19: letra_ffd port map(
           CLK=>CLK,
			   letra=>a23,
			  Q_mux=>i19
			-- Q_disp=>Q_disp
			  );

	letra_ffd20: letra_ffd port map(
           CLK=>CLK,
			   letra=>a24,
			  Q_mux=>i20
			-- Q_disp=>Q_disp
			  );

	letra_ffd21: letra_ffd port map(
           CLK=>CLK,
			   letra=>a25,
			  Q_mux=>i21
			-- Q_disp=>Q_disp
			  );
			  

	letra_ffd22: letra_ffd port map(
           CLK=>CLK,
			   letra=>a26,
			  Q_mux=>i22
			-- Q_disp=>Q_disp
			  );

	letra_ffd23: letra_ffd port map(
           CLK=>CLK,
			   letra=>a27,
			  Q_mux=>i23
			-- Q_disp=>Q_disp
			  );

	letra_ffd24: letra_ffd port map(
           CLK=>CLK,
			   letra=>a28,
			  Q_mux=>i24
		--	 Q_disp=>Q_disp
			  );
			  
	process(CLK)
		begin

			IF clk_desp2'event AND clk_desp2='1' THEN
				IF led="00" THEN
					Q_disp<=led;	
					--Q_mux<=i0;
					led<=led + "01";
				end if;
				IF led="01" THEN
					Q_disp<=led;
				--	Q_mux<=i1;
					led<=led + "01";
				end if;
				IF led="10" THEN
					Q_disp<=led;
				--	Q_mux<=i2;
					led<=led + "01";
				end if;
				IF led="11" THEN
					Q_disp<=led;
				--	Q_mux<=i3;
				led<="00";
				end if;
			END IF;
		end process;
		
	
	
end Behavioral;

