--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:32:39 12/06/2015
-- Design Name:   
-- Module Name:   C:/.Xilinx/letrero_movil/muxtest.vhd
-- Project Name:  letrero_movil
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: mux
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY muxtest IS
END muxtest;
 
ARCHITECTURE behavior OF muxtest IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mux
    PORT(
         a : IN  std_logic_vector(31 downto 0);
         s0 : IN  std_logic;
         s1 : IN  std_logic;
         s2 : IN  std_logic;
         s3 : IN  std_logic;
         s4 : IN  std_logic;
         y : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal a : std_logic_vector(31 downto 0) := (others => '0');
   signal s0 : std_logic := '0';
   signal s1 : std_logic := '0';
   signal s2 : std_logic := '0';
   signal s3 : std_logic := '0';
   signal s4 : std_logic := '0';

 	--Outputs
   signal y : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant k : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mux PORT MAP (
          a => a,
          s0 => s0,
          s1 => s1,
          s2 => s2,
          s3 => s3,
          s4 => s4,
          y => y
        );

   -- Clock process definitions
   

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

     

      -- insert stimulus here 
      a(8)<='1';
		a(1)<='0';
		a(2)<='1';
		wait for 25 ns;
		s0<='0';
		s1<='0';
		s2<='0';
		s3<='1';
		s4<='0';
		wait for 25 ns;
		s0<='1';
		s1<='0';
		s2<='0';
		s3<='0';
		s4<='0';
		wait for 50 ns;
		s0<='0';
		s1<='1';
		s2<='0';
		s3<='0';
		s4<='0';
      wait;
   end process;

END;
